import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Triangle triangle = new Triangle();
        triangle.initialize(sc);
        System.out.println("Triangle's perimeter is : ");
        System.out.println(triangle.findPerimeter(triangle));

    }
}
