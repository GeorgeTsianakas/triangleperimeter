import java.util.Scanner;

public class Triangle {

    private double a, b, c;

    public Triangle() {
    }

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Triangle initialize(Scanner sc) {
        System.out.println("Give point a");
        double a = sc.nextDouble();
        Triangle.this.setA(a);
        System.out.println("Give point b");
        double b = sc.nextDouble();
        Triangle.this.setB(b);
        System.out.println("Give point c");
        double c = sc.nextDouble();
        Triangle.this.setC(c);
        System.out.println("Triangle Initialized");
        return this;
    }

    public double findPerimeter(Triangle triangle) {
        double a = Triangle.this.getA();
        double b = Triangle.this.getB();
        double c = Triangle.this.getC();
        return (a + b + c);
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

}
